sap.ui.define([
    "com/ventia/security_app/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "com/ventia/security_app/common/fragment.definition",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "com/ventia/security_app/common/dependencies/xlsx/jszip",
    "com/ventia/security_app/common/dependencies/xlsx/xlsx",
  ], function(Controller, JSONModel, FragmentDefinition, MessageToast, MessageBox, _JSZIP, _XLSX) {
        class Import {
            constructor(source, controller) {
                this.controller = controller;
                this.source = source;
                this.error = [];
            }
            template() {
                let oWorkbook = XLSX.utils.book_new();
                oWorkbook.SheetNames.push("Template");
                let aName = [];
                let aType = [];
                for (let column of this.controller.table.columns) {
                    aName.push(`${column.name}`);
                    aType.push(`?${column.setting.type}`);
                }
                let data = XLSX.utils.aoa_to_sheet([aName, aType]);
                oWorkbook.Sheets["Template"] = data;
                XLSX.writeFile(oWorkbook, `${this.controller.table.name}.xlsx`, {
                    bookType: "xlsx", 
                });
            }
            download(oData) {
                return new Promise((resolve, reject) => {
                    try {
                        let oWorkbook = XLSX.utils.book_new();
                        oWorkbook.SheetNames.push(this.controller.table.name);
                        let aRows = [[]];
                        for (let column of this.controller.table.columns) {
                            aRows[0].push(`${column.name}`);
                        }
                        for (let oItem of oData) {
                            let temp = [];
                            let keys = Object.keys(oItem);
                            for (let key of keys) {
                                let index = aRows[0].indexOf(key);
                                temp[index] = oItem[key];
                            }
                            aRows.push(temp);
                        }
                        let data = XLSX.utils.aoa_to_sheet(aRows);
                        oWorkbook.Sheets[this.controller.table.name] = data;
                        XLSX.writeFile(oWorkbook, `${this.controller.table.name}.xlsx`, {
                            bookType: "xlsx", 
                        });
                        resolve();
                    } catch (error) {
                        reject(error);
                    }
                })
            }
            filter(primary_keys) {
                return new Promise((resolve, reject) => {
                    try {
                        var oFile = this.source.oFileUpload.files[0];
                        var iFileReader = new FileReader();
                        let filter = [];
                        let aSheets = [];
                        iFileReader.onload = (e) => {
                            var data = e.target.result;
                            var oWorkbook = XLSX.read(data, {
                                type: 'binary'
                            });
                            for (let sSheetName of oWorkbook.SheetNames) {
                                var oJS = XLS.utils.sheet_to_row_object_array(oWorkbook.Sheets[sSheetName]);
                                aSheets.push(oJS);
                            }
                            for (let rows of aSheets) {
                                for (var i = 0; i < rows.length; i++) {
                                    let row = rows[i];
                                    let _filter = [];
                                    for (var n = 0; n < primary_keys.length; n++) {
                                        let key = primary_keys[n];
                                        _filter.push(new sap.ui.model.Filter({
                                            path: key,
                                            operator: sap.ui.model.FilterOperator.EQ,
                                            value1: row[key]
                                        }))
                                    }
                                    filter.push(new sap.ui.model.Filter({
                                        filters: _filter,
                                        and: true
                                    }));
                                }
                            }
                            resolve(new sap.ui.model.Filter({
                                filters: filter,
                                and: false
                            }));
                        };
                        oFile ? iFileReader.readAsBinaryString(oFile) : resolve();
                    } catch (e) {
                        reject(e);
                    }
                })
            }
            xlsx() {
                return new Promise((resolve, reject) => {
                    try {
                        var oFile = this.source.oFileUpload.files[0];
                        var iFileReader = new FileReader();
                        let aSheets = [];
                        iFileReader.onload = (e) => {
                            var data = e.target.result;
                            var oWorkbook = XLSX.read(data, {
                                type: 'binary'
                            });
                            for (let sSheetName of oWorkbook.SheetNames) {
                                var oJS = XLS.utils.sheet_to_row_object_array(oWorkbook.Sheets[sSheetName]);
                                aSheets.push(oJS);
                            }
                            this.fBatch(aSheets).then(blocks => {
                                resolve({
                                    blocks: blocks,
                                    aError: this.error
                                });
                            }).catch(e => {
                                throw e;
                            });
                        };
                        oFile ? iFileReader.readAsBinaryString(oFile) : resolve();
                    } catch (e) {
                        reject(e);
                    }
                })
            }
            fError(msg) {
                this.error.push(msg);
            }
            fBlock(aContext) {
                let size = 10;
                return aContext.reduce((result, item, index) => {
                    const block = Math.floor(index/size);
                    if (!result[block]) result[block] = [];
                    result[block].push(item);
                    return result;
                }, []);
            }
            fBatch(aSheets) {
                return new Promise(resolve => {
                    let columns = this.controller.table.columns;
                    let aRows = [];
                    for (let rows of aSheets) {
                        for (let [i, row] of rows.entries()) {
                            let keys = Object.keys(row);
                            let data = {};
                            for (let key of keys) {
                                let column = columns.find(e => e.name === key);
                                if (column) {
                                    let type = column.setting.type;
                                    let _type = this.controller.EDM.get(type)().type;
                                    try {
                                        data[key] = _type(row[key]);
                                    } catch (e) {
                                        this.fError(`Spreadsheet row ${i}, column ${key} contains an invalid value.`)
                                    }
                                } else {
                                    this.fError(`Spreadsheet header-row contains a name ${key} that does not match the database.`)
                                }
                            }
                            aRows.push(data);
                        }
                    }
                    resolve(this.fBlock(aRows));
                })
            }

        }
        return Import;
    }
);